#!/bin/bash

echo "[ INFO ] $0 is starting..."

sudo apt update

sudo apt install -y openssl
sudo apt install -y nodejs
sudo apt install -y build-essential
sudo apt install -y clang
sudo apt install -y libxml2-dev

npm install -g ionic
npm install

echo "[ INFO ] increasing the file watch limit on system..."
echo "fs.inotify.max_user_watches=16384" >> sudo tee -a /etc/sysctl.conf
sudo sysctl -p

echo "[ INFO ] $0 has finished."
