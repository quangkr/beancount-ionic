import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickAddTxPage } from './quick-add-tx.page';

describe('QuickAddTxPage', () => {
  let component: QuickAddTxPage;
  let fixture: ComponentFixture<QuickAddTxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickAddTxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickAddTxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
