# Beancount Ionic

Beancount Ionic is a project to create a Progressive Web Application (PWA) using the Ionic Framework for the Beancount Double-Entry Accounting System. This project is part of the ARF Beancount Projects Collection.

This project is currently **NOT** ready for end-user use! But I hope you're excited about this project and I welcome feedback (including supportive messages) if you wish to send any my way!

## What is this project about?

Mobile entry into Beancount files is painful. It's even more painful if you 
apply complex organization structure, like I do, and version control all of your
data in Git, like I do.

Fava is perhaps the best known additional tool for Beancount, which provides
lots of added functionality and visualizations, but isn't great for mobile.

`beancount-ionic` is an attempt at providing a progressive web app (PWA) that
hides the dirty business of dealing with Git, while providing an easy to use
a mobile device for interfacing with Beancount text-based data files.

[Ionic Framework](https://ionicframework.com/) is the primary SDK used to create
this app, allowing `beancount-ionic` to work on the web, Android, and iOS
devices while providing a rich and pleasant user interface.

## Developer Getting Started

1. Install pre-requisite development tools:
    - `git`
    - `npm`/`nodejs`
1. Clone this Git repository to your development computer.
   1. `git clone git@gitlab.com:alex_ford/beancount-ionic.git`
2. Run dev setup script: `./scripts/setup-dev.sh`
2. Start Visual Studio Code using one of the `start-vscode.ps1` (for Windows) or `start-vscode.sh` (for Linux) scripts.
3. Proceed to install all recommended VS Code extensions defined by the workspace.
4. Using the terminal (either from VS Code or externally), enter the `./app/` directory.
5. Type `ionic build`
6. Type `ionic serve`

### Ionic CLI

Use the `ionic` CLI from within the `./app/` directory.

## Ionic Framework

- [Ionic Documentation](https://beta.ionicframework.com/docs/)
- [Scaffold Out Your App](https://beta.ionicframework.com/docs/building/scaffolding)
- [Change Your App Layout](https://beta.ionicframework.com/docs/layout/structure)
- [Theme Your App](https://beta.ionicframework.com/docs/theming/basics)

## About The Author/Developer

My name is Alex, and I am a professional Software Engineer by day, and enjoy continuing to develop software by night! Feel free to stop on by my website: [alexrichardford.com](http://www.alexrichardford.com)
